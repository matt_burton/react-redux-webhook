import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux'

import { auth } from 'firebase.js'
import { setUserlevelcons } from 'utilities' //Initial user setup

import Homepage from 'pages/Homepage/Homepage'
import Admin from 'pages/Control/Controller'
import Welcome from 'pages/Welcome/Welcome'


import Header from 'components/header/header'


class App extends Component {

	componentWillMount() {
		auth.onAuthStateChanged((user) => {
		if(user) {
			setUserlevelcons(user.email); //Set user level to either user / admin defined at: utilities/user/userq.js
			this.setState({ user });
		} 
		});
	}

	render() {

		const { user } = this.props
		return (
			<BrowserRouter>
				<Header />
				<div className="container">
				<Switch>
					{user.username && user.isAdmin &&
					<>
						<Route exact path="/" component={Admin} />
					</>
					}
					{user.username ?
						<>
							<Route exact path="/" component={Homepage} />
						</>
					:
						<Route path="/" component={Welcome} />
					}
				</Switch>
				</div>
			</BrowserRouter>
		)
	}
}

const mapStateToProps = state => ({
	  user: state.user
})

export default connect(mapStateToProps, {})(App)