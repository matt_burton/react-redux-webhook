import React, { Component } from 'react'

import Login from './Login/Login'
import Register from './Register/Register'


import styles from './Welcome.module.scss'

class Welcome extends Component {

    constructor(){
        super()
        this.state = {
            forms: {
                login: true,
                register: true
            },
            browserWidth: 0
        }
    }

    componentDidMount(){
        window.addEventListener('load', this.formState)
    }

    

  
    render() {

        const { showBoth, forms } = this.state

        const spreadProps = {
            toggleForm: this.toggleForm,
            showBoth,
            forms
        }

        return (
            <>
                <section className={styles.welcome}>
                    <div className={styles.introText}>
                   
                                <div className={styles.welcomeCopy}>
                                <h1>Welcome</h1>
                                <p>Test, quis rhoncus libero.</p>
                                </div>
                          
                    </div>
                    </section>
                    <section >
                    
                                <Login {...spreadProps} />
                                <Register {...spreadProps} />
                     
                    </section>
           
                <footer>
               
                </footer>
            </>
        )
    }

}
export default Welcome