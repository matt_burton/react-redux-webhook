import React from 'react'
import { connect } from 'react-redux'

import Input from '../../../components/forms/input'
import handleChange from '../../../utilities/input_hook'

import styles from '../../../components/forms/Form.module.scss'
import Button from '../../../components/ui/button/button'

import { setUserDetails  } from '../../../utilities' //Initial user setup

const UserFormstyled = ({user}) => {

    const saveDetails = e => {
			console.log('username',username);

			let sendObj = {'email': username, 'Firstname': firstname, 'Surname': surname}
			setUserDetails(sendObj);
	}


	const { value: username, bind: bindEmail, setArg: setUsername } = handleChange('');
	const { value: firstname, bind: bindFirstname } = handleChange('');
	const { value: surname, bind: bindSurname } = handleChange('');
	if(username === ''){
		setUsername(user.username);
	}


    return (
		<>
        <h1>Hi </h1>
        <p>Please enter some details below....</p>
		<div className="center">
        		 <form onSubmit={saveDetails} className={styles.theForm} autoComplete="off">
					{/* <form className={styles.theForm} autoComplete="off"> */}
						<label htmlFor="registerUsername">Email address</label>			
							<Input type="email" label="Email" fieldName="email" value={username} {...bindEmail} />
							<label htmlFor="registerPassword">First name:</label>			
							<Input type="text" label="First name" fieldName="password" value={firstname} {...bindFirstname} />
							<label htmlFor="registerPassword">Surname:</label>			
							<Input type="text" label="Surname" fieldName="password" value={surname} {...bindSurname} />
						<Button type="submit" label="Submit details" classes={['blue']} />
					</form>
       </div>
	   </>
    )}

const mapStateToProps = state => ({
	user: state.user,
})

export default connect(mapStateToProps, {})(UserFormstyled)