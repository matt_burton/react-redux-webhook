import React from 'react'

import { indexToLetter } from 'config/config'

import styles from './currentlist.module.scss'

const CurrentList = ({ title, description, questions }) => (
    <div className={styles.contents}>
        <h2>{title}</h2>
        <p className={styles.description}>{description}</p>
        <div className={styles.thePoll}>
            {questions.map(({ name, Number }, index) => (
                <div key={index} className={styles.answer}> 
                    <div className={styles.text}>
                        <span>{indexToLetter(index)}</span><p>{name}</p>
                    </div>
                    <div className={styles.votes}>
                        <span className={styles.voteTotal}>{Number} vote{(Number !== 1 ? 's' : '')}</span>
                    </div>
                </div>
            ))}
        </div>
    </div>
)

export default CurrentList