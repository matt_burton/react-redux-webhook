
export const populateCurentpoll = (questions) => {
    let title; let description; let question; let curID;
    if(!questions.currentState || questions.currentState === 'notStarted'){      //display welcome message - not started!
        title = "Polling is closed";
        description = "The poll has not been started yet, please wait - this screen will automatically refresh when the poll is open";
        questions = [];
    } else {
         if(questions.singleQuestion) {
            //  console.log(questions.singleQuestion);
             title = questions.singleQuestion.Title;
        //     curID = currentQ[0].id;
             description = questions.singleQuestion.Description;
             questions = questions.singleQuestion.questionsArray;
         }
    }
    const objReturn = {title: title, description: description, questions: questions}

    return objReturn
    
};
