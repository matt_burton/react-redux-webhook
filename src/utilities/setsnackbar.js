import { setSnackbar } from "../actions/snackbarActions"
import { store } from "../store"


export const setSnackbar_nfb = (type,message) => {
    //Set Interval
    let setInter = setInterval(function(){ 
        store.dispatch(setSnackbar({ currentStatus: false, message: message })); 
        clearInterval(setInter); 
    }, 5000);
    store.dispatch(setSnackbar({ type: type, currentStatus: true, message: message }));
}
