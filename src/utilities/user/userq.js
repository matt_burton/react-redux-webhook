import {store} from "../../store"
import {setUserLevel, setUsers} from "../../actions/userActions"
import firebase from '../../firebase.js';



export const getUserDetails = (userEmail) => {
    let userlevel ='user';
    if(userEmail === 'webape2@gmail.com'){
        userlevel = 'admin';
    } 
    let allusers = firebase.firestore().collection('users'); //Get reference from firebase
    allusers.where("email", "==", userEmail)
    .get()
    .then(function(querySnapshot) {
        if(querySnapshot.docs.length === 0){
            //set new user as true
            store.dispatch(setUserLevel(userEmail, 'nouserdetails'));
        } else {
            querySnapshot.forEach(function(doc) {
            store.dispatch(setUsers( doc.data()));
            store.dispatch(setUserLevel(userEmail, userlevel));
             });
            }
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
    });

}

export const setUserDetails = (userDetails) => {
    let allusers = firebase.firestore().collection('users'); //Get reference from firebase
    allusers.add(userDetails);
}


export const setUserlevelcons = (username) => {

}
