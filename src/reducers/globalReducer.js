import { 
    SET_PAGETITLE,
    TOGGLE_MENU,
    CURRENT_SLIDE
} from 'actions/types'


const initialState = {
    titles: {
        primary: '',
        secondary: ''
    },
    theme: '',
    isMenuOpen: false,
    currentSlide: false
}

export default function(state = initialState, action){
    switch(action.type){
        case SET_PAGETITLE:
            const { primary, secondary } = action.payload.titles
            return {
                ...state,
                titles: {
                    primary: (primary ? primary : ''),
                    secondary: (secondary ? secondary : ''),
                },
                theme: (action.payload.theme ? action.payload.theme : '')
            }
        case TOGGLE_MENU:
            return {
                ...state,
                isMenuOpen: (action.payload === 'close' ? false : !state.isMenuOpen)
            }
        case CURRENT_SLIDE:
            return {
                ...state,
                currentSlide: !state.currentSlide
            }
        default:
            return state
    }
}