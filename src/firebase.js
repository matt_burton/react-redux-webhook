import firebase from 'firebase'
const config = {
  apiKey: "AIzaSyBPJoVNmiOZZJqwbmnHyC5-Yhs_114vv1k",
  authDomain: "defaultproject-da8bb.firebaseapp.com",
  databaseURL: "https://defaultproject-da8bb.firebaseio.com",
  projectId: "defaultproject-da8bb",
  storageBucket: "defaultproject-da8bb.appspot.com",
  messagingSenderId: "339461951018",
  appId: "1:339461951018:web:69cb34c44394811ff6fd53"
};
firebase.initializeApp(config);
export const provider = new firebase.auth.GoogleAuthProvider();
export const auth = firebase.auth();
export default firebase;