import React from 'react'

const Input = ({ fieldName, onChange, value, type }) => {
    return (

            <input 
                id={fieldName} 
                name={fieldName} 
                type={(!type ? 'text' : type)} 
                value={value}
                onChange={onChange}
            />

    )
}
export default Input