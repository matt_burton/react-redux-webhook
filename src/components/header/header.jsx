import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { auth } from 'firebase.js'

import { setUserlevelcons, getUserDetails } from 'utilities' //Initial user setup

import { userLogout } from 'actions/userActions'

import PrimaryTitle from './primary-title/primary-title'
import Back from './back/back'


import MenuNavigation from './menu-navigation/menu-navigation';

class Header extends Component {

	constructor(){
		super()
		this.state = {
			isScrolled: false
		}
	}

	componentWillMount(){
			//Set user level based on username
			auth.onAuthStateChanged(user => {
				if (user) {
					setUserlevelcons(user.email); //Set current user details
					getUserDetails(user.email)
				}
			})
	}

	render() {
		return (
			<header>
							<Back /> 
							{/* <Back/> - Mobile back button  */}
							<PrimaryTitle />
							{/* <Primary title /> - Shows site title and current speaker(s)  */}
							<MenuNavigation />
							{/* <MenuNavigation /> - Mobile/Desktop links */}
			</header>
		)
	}
}

const mapStateToProps = state => ({
	user: state.user,
	global: state.global
})

export default withRouter(connect(mapStateToProps, { userLogout })(Header))