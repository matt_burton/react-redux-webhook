import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'


import styles from '../header.module.scss'

const PrimaryTitle = () => {

    
    
    return (
    <div className={styles.siteTitle}>
		    <h3><Link to="/">Site title</Link></h3>
    </div>
)}

const mapStateToProps = state => ({
 
})

export default connect(mapStateToProps, {})(PrimaryTitle)